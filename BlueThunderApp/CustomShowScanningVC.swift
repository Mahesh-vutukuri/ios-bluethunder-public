//
//  CustomShowScanningVC.swift
//  BlueThunderApp
//
//  Created by Sri Vutukuri on 4/21/17.
//  Copyright © 2017 Flex. All rights reserved.
//

import Foundation
import UIKit

class CustomShowScanningVC: UIStoryboardSegue {
    
    override func perform() {
        source.navigationController?.pushViewController(destination, animated: true)
    }
}
