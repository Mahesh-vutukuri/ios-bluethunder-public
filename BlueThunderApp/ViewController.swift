//
//  ViewController.swift
//  BlueThunderApp
//
//  Created by Sri Vutukuri on 4/21/17.
//  Copyright © 2017 Flex. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func welcomAction(_ sender: Any) {
        // Show the bluetooth permission if not allowed
        NSLog("Show BT permission")
        
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        
        if (appDelegate.btManager?.isPoweredOn())! {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let scanningVC = storyBoard.instantiateViewController(withIdentifier: "ScanningViewController")
            self.navigationController?.pushViewController(scanningVC, animated: true)
            
        } else {
            NSLog("Bluetooth is powered OFF")
            
            let alert = UIAlertController(title: "Bluetooth Disabled", message: "Turn ON Bluetooth before proceeding to scan for devices.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

}

