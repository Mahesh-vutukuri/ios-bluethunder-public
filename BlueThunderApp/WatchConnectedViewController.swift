//
//  WatchConnectedViewController.swift
//  BlueThunderApp
//
//  Created by Sri Vutukuri on 4/24/17.
//  Copyright © 2017 Flex. All rights reserved.
//

import UIKit
import CoreBluetooth
import BlueThunder

class WatchConnectedViewController: UIViewController {

    @IBOutlet weak var readButton: UIButton!
    @IBOutlet weak var writeButton: UIButton!
    @IBOutlet weak var watchConnectedLabel: UILabel!
    
    
    @IBOutlet weak var pairingLabel: UILabel!
    @IBOutlet weak var pairingDescriptionLabel: UILabel!
    
    @IBOutlet weak var pairingView: UIView!
    @IBOutlet weak var connectionErrorView: UIView!
    
    @IBOutlet weak var successLabel: UILabel!
    
    @IBOutlet weak var countLabel: UILabel!
    
    var discoveredPeripheral: CBPeripheral? = nil
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var btManager: BTManager!
    
    var countNumber = 1
    
    // REFACTOR
    let peripheral_service_uuid = CBUUID(string: "550f79c2-a6d3-4841-a4d7-b2aba08bce6f")
//    let encrypted_characteristic_uuid = CBUUID(string: "84DBF9F7-381F-4B33-8BE4-18398C7F6E89")
//    let unencrypted_characteristic_uuid = CBUUID(string: "2B05165E-7CC3-47BF-AEA3-5F317576880C")
//    let unencrypted_write_characteristic_uuid = CBUUID(string: "6CF6BC2B-95EF-418E-8470-555272B02E13")

    let encrypted_characteristic_uuid = CBUUID(string: "550f79c2-a6d3-4841-a4d7-b2aba08bce6f")
    let unencrypted_characteristic_uuid = CBUUID(string: "772AE377-B3D2-4F8E-4042-5481D1E0098C")
    let unencrypted_write_characteristic_uuid = CBUUID(string: "772AE377-B3D2-4F8E-4042-5481D1E0098C")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.hidesBackButton = true
        
        initialSetup()
        
        NotificationCenter.default.addObserver(self, selector: #selector(pairingError), name: NSNotification.Name(rawValue: "PairingError"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(pairingSuccess), name: NSNotification.Name(rawValue: "PairingSuccess"), object: nil)
        
        btManager = appDelegate.btManager
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initialSetup() {
        hideWatchConnectedView()
        
        connectionErrorView.isHidden = true
        connectionErrorView.isUserInteractionEnabled = false
        
        successLabel.isHidden = true
        
        self.navigationItem.title = "Pairing"

        showPairingView()
    }

    func pairingError() {
        showConnectionError()
    }
    
    func pairingSuccess() {
        hidePairingView()
        successLabel.isHidden = false
        self.navigationItem.title = "Connected"
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
            self.setupWatchConnectedView()
            self.navigationItem.title = "Flex"
        }
    }
    
    @IBAction func readAction(_ sender: Any) {
        // Read the characteristic Value
        
        discoveredPeripheral = btManager.getPairedDevice()
        
        // REFACTOR
        if let readChar = btManager.getDiscoveredCharacteristic(uuid: unencrypted_characteristic_uuid) {
            btManager.readCharacteristic(readChar, closure: { (status, value) in
                if status == .Success {
                    DispatchQueue.main.async {
                        var dataDescription = "NA"
                        if value != nil {
                            var out = 0
                            if (value!.count) > 0 {
                                 out = value!.withUnsafeBytes({ (ptr: UnsafePointer<Int>) -> Int in
                                    return ptr.pointee
                                })
                            }
                            dataDescription = "\(out)"
                        }
                        NSLog("Read value is: \(dataDescription)")
                        
                        self.countLabel.text = ""
                        self.countLabel.text = dataDescription 
                    }
                }
            })
        }
    }
    
    
    @IBAction func writeAction(_ sender: Any) {
        // Increment the characteristic value
        
        discoveredPeripheral = btManager.getPairedDevice()
        
        // REFACTOR
        if let writeChar = btManager.getDiscoveredCharacteristic(uuid: unencrypted_write_characteristic_uuid) {
            countNumber = countNumber + 1
            let data = NSData(bytes: &countNumber, length: MemoryLayout<Int>.size) as Data
            // discoveredPeripheral?.writeValue(data, for: writeChar, type: .withResponse)
            btManager.writeCharacteristic(writeChar, data: data, closure: { (status, data) in
                if status == .Success {
                    NSLog("Wrote data successfully: \(data)")
                }
            })
        }
    }
    
    @IBAction func restartAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: - UI helper methods
    
    func showPairingView() {
        pairingView.alpha = 1.0
        pairingView.isHidden = false
        pairingView.isUserInteractionEnabled = true
        
        
        pairingLabel.isHidden = false
        pairingLabel.isEnabled = true
        
        pairingDescriptionLabel.isHidden = false
        pairingDescriptionLabel.isEnabled = false
    }
    
    func hidePairingView() {
        
        pairingView.isHidden = true
        pairingView.isUserInteractionEnabled = false
        
        pairingLabel.isHidden = true
        pairingLabel.isEnabled = false
        
        pairingDescriptionLabel.isHidden = true
        pairingDescriptionLabel.isEnabled = false
    }
    
    
    func showConnectionError() {
        hidePairingView()
        hideWatchConnectedView()
        
        connectionErrorView.alpha = 1.0
        connectionErrorView.isHidden = false
        connectionErrorView.isUserInteractionEnabled = true
    }
    
    func hideConnectionError() {
        connectionErrorView.isHidden = true
        connectionErrorView.isUserInteractionEnabled = false
    }

    
    func setupWatchConnectedView() {
        readButton.isHidden = false
        readButton.isEnabled = true
        
        writeButton.isHidden = false
        writeButton.isEnabled = true
        
        watchConnectedLabel.isHidden = false
        watchConnectedLabel.isEnabled = false
        
        successLabel.isHidden = true
    }
    
    func hideWatchConnectedView() {
        readButton.isHidden = true
        readButton.isEnabled = false
        
        writeButton.isHidden = true
        writeButton.isEnabled = false
        
        watchConnectedLabel.isHidden = true
        watchConnectedLabel.isEnabled = false

    }
}
