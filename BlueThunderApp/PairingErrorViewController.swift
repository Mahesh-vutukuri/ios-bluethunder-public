//
//  PairingErrorViewController.swift
//  BlueThunderApp
//
//  Created by Sri Vutukuri on 4/24/17.
//  Copyright © 2017 Flex. All rights reserved.
//

import Foundation
import UIKit

class PairingErrorViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func restartAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
