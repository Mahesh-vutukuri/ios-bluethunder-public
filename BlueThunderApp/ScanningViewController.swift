//
//  ScanningViewController.swift
//  BlueThunderApp
//
//  Created by Sri Vutukuri on 4/21/17.
//  Copyright © 2017 Flex. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth
import BlueThunder

class ScanningViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var scanTableView: UITableView!
    
    var btManager: BTManager?
    
    var discoveredPeripehral: CBPeripheral?
    var discoveredPeripheralList: Set<CBPeripheral>?
    
    
    let peripheral_service_uuid = CBUUID(string: "550f79c2-a6d3-4841-a4d7-b2aba08bce6f")
    //let encrypted_characteristic_uuid = CBUUID(string: "84DBF9F7-381F-4B33-8BE4-18398C7F6E89")
    let encrypted_characteristic_uuid = CBUUID(string: "772AE377-B3D2-4F8E-4042-5481D1E0098C")
    // let unencrypted_characteristic_uuid = CBUUID(string: "2B05165E-7CC3-47BF-AEA3-5F317576880C")
    // let ack_characteristic_uuid = CBUUID(string: "7AA907E3-F268-4077-A79A-B46FD40BBEFF")
    
    @IBOutlet weak var rescanButton: UIButton!
    var scanTimer: Timer?
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.hidesBackButton = true
        self.navigationItem.title = "Pairing"
        
        btManager = (UIApplication.shared.delegate as! AppDelegate).btManager
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideScanButton()
        discoveredPeripheralList = []
        showActivityIndicator()
        
        NSLog("Starting to scan for devices")
        scan()
        
        // Start a timer to show rescan button
        Timer.scheduledTimer(withTimeInterval: 3.0, repeats: true) { (timer) in
            self.showScanButton()
            self.hideActivityIndicator()
        }
    }
    
    
    @IBAction func rescanAction(_ sender: Any) {
        cleanup()
        scan()
        hideScanButton()
        showActivityIndicator()
    }
    
    func hideScanButton() {
        rescanButton.isHidden = true
        rescanButton.isEnabled = false
    }
    
    func showScanButton() {
        rescanButton.isHidden = false
        rescanButton.isEnabled = true
    }
    
    func showActivityIndicator() {
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
    }
    
    func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }
    
    func scan() {
        btManager?.discoverDevices(serviceUUID: peripheral_service_uuid, timeout: 5, closure: { (status, list) in
            if status == .Success {
                self.discoveredPeripheralList?.insert(list.first!)
                
                DispatchQueue.main.async {
                    self.scanTableView.reloadData()
                }
            }
        })
    }
    
    func cleanup() {
        discoveredPeripheralList = []
    }

    
    // MARK: - Table view delegate methods
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScanCell", for: indexPath)
        
        if (discoveredPeripheralList?.count)! < 0 {
            return cell
        }
        
        let peripheral = discoveredPeripheralList?[(discoveredPeripheralList?.index((discoveredPeripheralList?.startIndex)!, offsetBy: indexPath.row))!]
        cell.textLabel!.text = peripheral?.name ?? "Unknown"
        // cell.detailTextLabel!.text = "\(String(describing: peripheral?.state))"
        return cell
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return discoveredPeripheralList?.count ?? 0
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if (discoveredPeripheralList?.count)! > 0 {
            let selectedPeripheral = discoveredPeripheralList?[(discoveredPeripheralList?.index((discoveredPeripheralList?.startIndex)!, offsetBy: indexPath.row))!]
            discoveredPeripehral = selectedPeripheral

            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let watchConnectedVC = storyBoard.instantiateViewController(withIdentifier: "WatchConnectedViewController")
            
            btManager?.pair(device: selectedPeripheral!, serviceUUID: peripheral_service_uuid, pairingCharacteristicUUID: encrypted_characteristic_uuid, closure: { (status) in
                
                if status == .Paired {
                    NSLog("Pairing Success")
                    // Transistion to the setting up screen and animate to the Connnected Screen
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PairingSuccess"), object: nil)
                    }
                } else if status == .PairingFailed {
                    // Transistion to the pairing error screen
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PairingError"), object: nil)
                    }

                }
            })
            
             self.navigationController?.pushViewController(watchConnectedVC, animated: true)
        }
    }
    
    
}
